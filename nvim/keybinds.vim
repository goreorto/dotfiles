let mapleader = ','

nnoremap <C-Left> :tabprevious<CR>
nnoremap <C-Right> :tabnext<CR>
map <C-j> <C-W>j
map <C-k> <C-W>k
map <C-h> <C-W>h
map <C-l> <C-W>l

map gn :bnext<CR>
map gb :bprevious<CR>
map <leader>n :NERDTreeToggle<CR>
map <leader>m :NERDTreeFind<CR>
map <leader>p :TagbarToggle<CR>
nnoremap <silent> <leader>t  <cmd>Telescope find_files<CR>
nnoremap <silent> <leader>gm <cmd>Telescope lsp_document_symbols<CR>
nnoremap <silent> <leader>gr <cmd>Telescope lsp_references<CR>
nnoremap <silent> <leader>gii <cmd>Telescope lsp_implementations<CR>
nnoremap <silent> <leader>d  <cmd>Telescope dap configurations<CR>
nnoremap <silent> <leader>b  <cmd>lua require'dapui'.toggle()<CR>
nnoremap <silent> <leader>bt <cmd>lua require'dap'.toggle_breakpoint()<CR>
nnoremap <silent> <leader>gp <cmd>Telescope diagnostics<CR>
nnoremap <silent> <leader>gi <cmd>lua vim.lsp.buf.hover()<CR>


"nmap <leader>t <plug>(fzf-maps-n)<
"xmap <leader>t <plug>(fzf-maps-x)
"omap <leader>t <plug>(fzf-maps-o)
map <leader>h :nohl<CR>

nnoremap <silent> <Leader>bd :Bclose!<CR>

" format json
nmap <Leader>f :%!python -m json.tool<CR>
" format xml
nmap <Leader>x :%!python3 -c "import xml.dom.minidom, sys; print(xml.dom.minidom.parse(sys.stdin).toprettyxml())"<CR>


