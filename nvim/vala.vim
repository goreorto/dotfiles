"let g:asyncomplete_auto_popup = 1
"let g:asyncomplete_popup_delay = 100
"let g:tagbar_ctags_bin = 'anjuta-tags'
"let vala_no_trail_space_error = 1
"let vala_space_errors = 1
"let vala_no_tab_space_error = 1

let g:lsp_settings = {
    \ 'vala-language-server': {
    \     'cmd': {server_info->[&shell, &shellcmdflag, 'vala-language-server']},
    \ },
\ }

"autocmd FileType vala ValaCodingStyle
"inoremap <expr> <Tab>   pumvisible() ? "\<C-n>" : "\<Tab>"
"inoremap <expr> <S-Tab> pumvisible() ? "\<C-p>" : "\<S-Tab>"
"inoremap <expr> <cr>    pumvisible() ? "\<C-y>" : "\<cr>"
"
" let g:tagbar_type_vala = {
"     \ 'ctagstype' : 'vala',
"     \ 'kinds'     : [
"         \ 'c:classes',
"         \ 'd:delegates',
"         \ 'e:enumerations',
"         \ 'E:error domains',
"         \ 'f:fields',
"         \ 'i:interfaces',
"         \ 'm:methods',
"         \ 'p:properties',
"         \ 'r:error codes',
"         \ 's:structures',
"         \ 'S:signals',
"         \ 'v:enumeration values'
"     \ ],
"     \ 'sro'        : '.',
"     \ 'kind2scope' : {
"         \ 'i' : 'interface',
"         \ 'c' : 'class',
"         \ 's' : 'structure',
"         \ 'e' : 'enum'
"     \ },
"     \ 'scope2kind' : {
"         \ 'interface' : 'i',
"         \ 'class'     : 'c',
"         \ 'struct'    : 's',
"         \ 'enum'      : 'e'
"     \ }
" \ }

" Restore cursor position, window position, and last search after running a
" command.
function! Preserve(command)
  " Save the last search.
  let search = @/

  " Save the current cursor position.
  let cursor_position = getpos('.')

  " Save the current window position.
  normal! H
  let window_position = getpos('.')
  call setpos('.', cursor_position)

  " Execute the command.
  execute a:command
  if v:shell_error > 0
    silent undo
    redraw
    echomsg 'formatprg "' .. &formatprg .. '" exited with status ' .. v:shell_error
  endif

  " Restore the last search.
  let @/ = search

  " Restore the previous window position.
  call setpos('.', window_position)
  normal! zt

  " Restore the previous cursor position.
  call setpos('.', cursor_position)
endfunction

" Specify path to your Uncrustify configuration file.
let g:uncrustify_cfg_file_path =
    \ shellescape(fnamemodify('~/.uncrustify.cfg', ':p'))

function! Uncrustify(language)
  call Preserve(':silent %!uncrustify'
      \ . ' -q '
      \ . ' -l ' . a:language
      \ . ' -c ' . g:uncrustify_cfg_file_path)
endfunction

"autocmd BufWritePre *.vala :call Uncrustify('vala')
