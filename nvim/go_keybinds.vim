augroup golang
	"au FileType go nmap <Leader>gt :GoTest<CR>
	"au FileType go nmap <Leader>gft :GoTestFunc<CR>
	"au FileType go nmap <Leader>gb :GoBuild<CR>
	"au FileType go nmap <Leader>gg :GoAlternate<CR>
	"au FileType go nmap <Leader>gd :GoDescribe<CR>
	"au FileType go nmap <Leader>gcc :GoCoverageClear<CR>:colorscheme tender<CR>
	"au FileType go nmap <Leader>gc :GoCoverage<CR>
	"au FileType go nmap <Leader>grn :GoRename<CR>
	"au FileType go nmap <Leader>ge :GoRun<CR>
	"au FileType go nmap <Leader>gi :GoInfo<CR>
	"au FileType go nmap <Leader>gr :GoReferrers<CR>
augroup end

