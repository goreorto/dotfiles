" Dependencies
Plug 'vim-scripts/L9', {'name':'L9'}

" UI
Plug 'scrooloose/nerdtree'
Plug 'Xuyuanp/nerdtree-git-plugin'
Plug 'nathanaelkane/vim-indent-guides'
Plug 'b3niup/numbers.vim'
Plug 'ianremmler/frood'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'edkolev/tmuxline.vim'

" Colors
"Plug 'fortes/vim-escuro'
Plug 'fatih/molokai'

" Utils
Plug 'scrooloose/nerdcommenter'
Plug 'tpope/vim-fugitive'
Plug 'vim-scripts/Align'
Plug 'easymotion/vim-easymotion'
Plug 'SirVer/ultisnips'
Plug 'tpope/vim-surround'

" autocomplete
Plug 'neovim/nvim-lspconfig'
Plug 'hrsh7th/cmp-nvim-lsp'
Plug 'hrsh7th/cmp-buffer'
Plug 'hrsh7th/cmp-path'
Plug 'hrsh7th/cmp-cmdline'
Plug 'hrsh7th/nvim-cmp'
Plug 'quangnguyen30192/cmp-nvim-ultisnips'

Plug 'nvim-lua/plenary.nvim'
Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}
Plug 'nvim-telescope/telescope.nvim', { 'tag': '0.1.4' }
Plug 'mfussenegger/nvim-dap'
Plug 'rcarriga/nvim-dap-ui'
Plug 'nvim-telescope/telescope-dap.nvim'
Plug 'williamboman/mason.nvim'
Plug 'williamboman/mason-lspconfig.nvim'
Plug 'mfussenegger/nvim-lint'
Plug 'rshkarin/mason-nvim-lint'

Plug 'bronson/vim-visual-star-search'
Plug 'majutsushi/tagbar'
Plug 'airblade/vim-gitgutter'
"Plug 'sheerun/vim-polyglot'
Plug 'powerline/powerline-fonts'
Plug 'github/copilot.vim'


" JSON
Plug 'elzr/vim-json'
Plug 'robbles/logstash.vim'

" Go
source ~/.config/nvim/go_plugins.vim

" Vala
source ~/.config/nvim/vala_plugins.vim

" Elixir
source ~/.config/nvim/elixir_plugins.vim

" Lint
Plug 'tpope/vim-repeat'
Plug 'tpope/vim-unimpaired'
