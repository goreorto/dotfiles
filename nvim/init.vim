let g:powerline_pycmd="python"
let laststatus=2
if &compatible
  set nocompatible
endif

call plug#begin(stdpath('data') . '/plugged')
source ~/.config/nvim/plugins.vim
call plug#end()

filetype plugin indent on
syntax enable

lua <<EOF
  require('mason').setup()
  require('mason-lspconfig').setup()
  require('lspconfig').gopls.setup{}
  require("mason-nvim-lint").setup({
    ensure_installaled = {'golangci-lint'}
  })
  require('telescope').setup()
  require('telescope').load_extension('dap')
  require("dapui").setup()
  -- require('dap-go').setup {
  --   -- Additional dap configurations can be added.
  --   -- dap_configurations accepts a list of tables where each entry
  --   -- represents a dap configuration. For more details do:
  --   -- :help dap-configuration
  --   dap_configurations = {
  --     {
  --       -- Must be "go" or it will be ignored by the plugin
  --       type = "go",
  --       name = "Attach remote",
  --       mode = "remote",
  --       request = "attach",
  --     },
  --   },
  --   -- delve configurations
  --   delve = {
  --     -- the path to the executable dlv which will be used for debugging.
  --     -- by default, this is the "dlv" executable on your PATH.
  --     path = "dlv",
  --     -- time to wait for delve to initialize the debug session.
  --     -- default to 20 seconds
  --     initialize_timeout_sec = 20,
  --     -- a string that defines the port to start delve debugger.
  --     -- default to string "${port}" which instructs nvim-dap
  --     -- to start the process in a random available port
  --     port = "33551",
  --     -- additional args to pass to dlv
  --     args = {},
  --     -- the build flags that are passed to delve.
  --     -- defaults to empty string, but can be used to provide flags
  --     -- such as "-tags=unit" to make sure the test suite is
  --     -- compiled during debugging, for example.
  --     -- passing build flags using args is ineffective, as those are
  --     -- ignored by delve in dap mode.
  --     build_flags = "",
  --   },
  -- }
  require'nvim-treesitter.configs'.setup {
    highlight = {
      enable = true,
      -- Setting this to true will run `:h syntax` and tree-sitter at the same time.
      -- Set this to `true` if you depend on 'syntax' being enabled (like for indentation).
      -- Using this option may slow down your editor, and you may see some duplicate highlights.
      -- Instead of true it can also be a list of languages
      additional_vim_regex_highlighting = false,
    },
  }

  local cmp = require 'cmp'
  cmp.setup({
    snippet = {
      -- REQUIRED - you must specify a snippet engine
      expand = function(args)
        vim.fn["UltiSnips#Anon"](args.body) -- For `ultisnips` users.
      end,
    },
    window = {
      -- completion = cmp.config.window.bordered(),
      -- documentation = cmp.config.window.bordered(),
    },
    mapping = cmp.mapping.preset.insert({
      ['<C-b>'] = cmp.mapping.scroll_docs(-4),
      ['<C-f>'] = cmp.mapping.scroll_docs(4),
      ['<C-Space>'] = cmp.mapping.complete(),
      ['<C-e>'] = cmp.mapping.abort(),
      ['<CR>'] = cmp.mapping.confirm({ select = true }), -- Accept currently selected item. Set `select` to `false` to only confirm explicitly selected items.
    }),
    sources = cmp.config.sources({
      { name = 'nvim_lsp' },
      { name = 'ultisnips' }, -- For ultisnips users.
    }, {
      { name = 'buffer' },
    })
  })

  -- Set configuration for specific filetype.
  cmp.setup.filetype('gitcommit', {
    sources = cmp.config.sources({
      { name = 'git' }, -- You can specify the `git` source if [you were installed it](https://github.com/petertriho/cmp-git).
    }, {
      { name = 'buffer' },
    })
  })

  -- Use buffer source for `/` and `?` (if you enabled `native_menu`, this won't work anymore).
  cmp.setup.cmdline({ '/', '?' }, {
    mapping = cmp.mapping.preset.cmdline(),
    sources = {
      { name = 'buffer' }
    }
  })

  -- Use cmdline & path source for ':' (if you enabled `native_menu`, this won't work anymore).
  cmp.setup.cmdline(':', {
    mapping = cmp.mapping.preset.cmdline(),
    sources = cmp.config.sources({
      { name = 'path' }
    }, {
      { name = 'cmdline' }
    })
  })

  -- Set up lspconfig.
  local lspconfig = require('lspconfig')
  local lsp_defaults = lspconfig.util.default_config

  lsp_defaults.capabilities = vim.tbl_deep_extend(
    'force',
    lsp_defaults.capabilities,
    require('cmp_nvim_lsp').default_capabilities()
  )
EOF

source ~/.config/nvim/functions.vim
source ~/.config/nvim/base.vim
source ~/.config/nvim/ui.vim
source ~/.config/nvim/keybinds.vim

" LANG SUPPORT
"   go
source ~/.config/nvim/go.vim
source ~/.config/nvim/go_keybinds.vim

"   vala
autocmd filetype vala source ~/.config/nvim/vala.vim
autocmd filetype vala source ~/.config/nvim/vala_keybinds.vim

"   elixir
"autocmd filetype exs,ex source ~/.config/nvim/elixir.vim
