set backspace=2
syntax sync minlines=256
set re=0
set lazyredraw " faster macro execution
set relativenumber
set hidden
"set backupdir=/tmp
set swapfile
set nowrap
set clipboard+=unnamedplus
set clipboard+=unnamed
set ignorecase " warning: affect substitution
set smartcase
set autoread
set autowrite
set ruler
set noexpandtab
set smarttab
set shiftwidth=4
set tabstop=4
set softtabstop=4
set ai
set si
set cmdheight=1
set laststatus=2
set scrolloff=10
set title
set wildignore=__pycache__,node_modules,bower_components,dev,public/releases
set list
set listchars=tab:·\ ,trail:·
"set foldmethod=indent
"set foldlevelstart=99
set foldmethod=expr
set foldexpr=nvim_treesitter#foldexpr()
set nofoldenable
set cursorline
set showmatch
set incsearch
set t_co=256
set mouse=n " scroll is normal mode only
set timeoutlen=150

syntax sync maxlines=500


let g:airline#extensions#ale#enabled = 1
let g:airline#extensions#tabline#enabled = 1
let g:airline_powerline_fonts = 1

autocmd bufwrite * :call Deletetrailingws()
