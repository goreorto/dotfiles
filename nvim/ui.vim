set encoding=utf-8

let g:NERDTreeWinPos = "left"
let NERDTreeIgnore = ['\.pyc$', '^__pycache__$', 'generated_\.go$']

autocmd VimEnter * if !argc() | NERDTree | endif
autocmd VimEnter * if isdirectory(expand('<afile>')) | NERDTree | endif
autocmd VimEnter,Colorscheme * :hi IndentGuidesOdd  guibg=red  ctermbg=3

set statusline+=%#warningmsg#
set statusline+=%*

let g:CommandTMaxHeight=10

if has('termguicolors')
  set termguicolors
endif

colorscheme molokai
let g:rehash256 = 1
let g:molokai_original = 1
"set background=dark

let g:indent_guides_enable_on_vim_startup = 1
let g:indent_guides_exclude_filetypes = ['help', 'nerdtree']

