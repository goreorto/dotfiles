set nocursorcolumn

set colorcolumn=120
"autocmd BufWritePre *.go lua vim.lsp.buf.format({ async = false })
lua <<EOF
  -- FORMAT ON SAVE
  vim.api.nvim_create_autocmd("BufWritePre", {
    pattern = "*.go",
    callback = function()
      local params = vim.lsp.util.make_range_params()
      params.context = {only = {"source.organizeImports"}}
      -- buf_request_sync defaults to a 1000ms timeout. Depending on your
      -- machine and codebase, you may want longer. Add an additional
      -- argument after params if you find that you have to write the file
      -- twice for changes to be saved.
      -- E.g., vim.lsp.buf_request_sync(0, "textDocument/codeAction", params, 3000)
      local result = vim.lsp.buf_request_sync(0, "textDocument/codeAction", params)
      for cid, res in pairs(result or {}) do
        for _, r in pairs(res.result or {}) do
          if r.edit then
            local enc = (vim.lsp.get_client_by_id(cid) or {}).offset_encoding or "utf-16"
            vim.lsp.util.apply_workspace_edit(r.edit, enc)
          end
        end
      end
      vim.lsp.buf.format({async = false})
    end
  })

  -- DELVE DAP CONFIG
  local dap = require("dap")
  dap.adapters.go = function(callback, config)
    -- local stdout = vim.loop.new_pipe(false)
    -- local handle
    -- local pid_or_err
    local port = 33551
    -- local opts = {
    --   stdio = { nil, stdout },
    --   args = { "connect", "127.0.0.1:" .. port },
    --   detached = true,
    -- }
    -- handle, pid_or_err = vim.loop.spawn("dlv", opts, function(code)
    --   stdout:close()
    --   handle:close()
    --   if code ~= 0 then
    --    print("dlv exited with code", code)
    --   end
    -- end)
    -- assert(handle, "Error running dlv: " .. tostring(pid_or_err))
    -- stdout:read_start(function(err, chunk)
    --   assert(not err, err)
    --   if chunk then
    --     vim.schedule(function()
    --       require("dap.repl").append(chunk)
    --     end)
    --   end
    -- end)
    vim.defer_fn(function()
      callback({ type = "server", host = "127.0.0.1", port = port })
    end, 100)
  end

  dap.configurations.go = {
    {
      type = "go", -- must match line 32 `adapter.{go}`
      name = "Remote",
      request = "attach",
      mode = "remote",
    },
    -- {
    --   type = "go",
    --   name = "Debug",
    --   request = "launch",
    --   program = "${file}",
    -- },
    -- {
    --   type = "go",
    --   name = "Debug test",
    --   request = "launch",
    --   mode = "test",
    --   program = "${file}",
    -- },
-- {
--   type = "go",
--    name = "Debug test (go.mod)",
--    request = "launch",
--    mode = "test",
--    program = "./${relativeFileDirname}",
-- },
  }

EOF

"let g:go_referrs_mode="guru" "https://github.com/fatih/vim-go/pull/2566
"let g:go_doc_popup_window=1
" let g:go_gopls_options = ['-tags=testdb', '-remote=auto']
"let g:go_snippet_engine = "ultisnips"
"let g:go_def_mode='gopls'
"let g:go_info_mode='gopls'
"let g:go_auto_sameids = 0
"let g:go_auto_type_info = 0
"let g:go_metalinter_command = 'golangci-lint'
"let g:go_metalinter_enabled = []
"let g:go_metalinter_autosave = 0
"let g:go_metalinter_autosave_enabled = []
"let g:go_def_reuse_buffer = 0
"let g:go_fmt_command = "goimports"
"let g:go_fmt_options = {
"\ 'goimports': '-local github.com/teamwork',
"\ }
"let g:go_fmt_autosave = 1
"let g:go_highlight_fields = 1
"let g:go_highlight_functions = 1
"let g:go_highlight_function_calls = 1
"let g:go_highlight_methods = 1
"let g:go_highlight_structs = 1
"let g:go_highlight_interfaces = 1
"let g:go_highlight_operators = 1
"let g:go_highlight_build_constraints = 1
"let g:go_highlight_extra_types = 1
"let g:go_list_type = 'locationlist'
"let g:go_build_tags = 'testdb'
"let g:go_debug = ['shell-commands']
"let g:tagbar_type_go = {
  "\ 'ctagstype' : 'go',
  "\ 'kinds'     : [
      "\ 'p:package',
      "\ 'i:imports:1',
      "\ 'c:constants',
      "\ 'v:variables',
      "\ 't:types',
      "\ 'n:interfaces',
      "\ 'w:fields',
      "\ 'e:embedded',
      "\ 'm:methods',
      "\ 'r:constructor',
      "\ 'f:functions'
  "\ ],
  "\ 'sro' : '.',
  "\ 'kind2scope' : {
      "\ 't' : 'ctype',
      "\ 'n' : 'ntype'
  "\ },
  "\ 'scope2kind' : {
      "\ 'ctype' : 't',
      "\ 'ntype' : 'n'
  "\ },
  "\ 'ctagsbin'  : 'gotags',
  "\ 'ctagsargs' : '-sort -silent'
"\ }

"let g:ale_linters = {
	"\ 'go': ['golangci-lint'],
	"\}
