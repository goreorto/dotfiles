import XMonad
import System.Exit

import Data.Ratio ((%))

import qualified XMonad.StackSet as W
import qualified Data.Map        as M

import XMonad.Config.Desktop
import XMonad.Hooks.SetWMName
import XMonad.Hooks.EwmhDesktops
import XMonad.Config.Gnome

import XMonad.Hooks.DynamicLog
import XMonad.Hooks.FloatNext
import XMonad.Hooks.ManageDocks
import XMonad.Hooks.ManageHelpers(doCenterFloat)

import XMonad.Layout.SimplestFloat
import XMonad.Layout.NoBorders
import XMonad.Layout.PerWorkspace
import XMonad.Layout.ResizableTile
import XMonad.Layout.BinarySpacePartition
import XMonad.Layout.Spacing
import XMonad.Layout.Gaps
import XMonad.Layout.ThreeColumns
import XMonad.Layout.Minimize

import XMonad.Actions.Minimize
import XMonad.Actions.FloatKeys

import XMonad.Util.SpawnOnce
import XMonad.Util.Run(spawnPipe)
import System.IO

toggleFloat = withFocused (\windowId -> do
                              { floats <- gets (W.floating . windowset);
                                if windowId `M.member` floats
                                then withFocused $ windows . W.sink
                                else do
                                     keysMoveWindowTo (1280, 720) (1%2, 1%2) windowId
                              }
                          )

getActiveLayoutDescription :: X String
getActiveLayoutDescription = do
    workspaces <- gets windowset
    return $ description . W.layout . W.workspace . W.current $ workspaces

-- codes https://hackage.haskell.org/package/xmonad-contrib-0.16/docs/XMonad-Config-Prime.html
myKeys conf@(XConfig {XMonad.modMask = modMask}) = M.fromList $

    -- launch a terminal
    [ ((modMask .|. shiftMask,   xK_Return), spawn $ XMonad.terminal conf)

    -- launch brave
    , ((modMask .|. shiftMask,   xK_b     ), spawn "exe=`brave`")

	-- launch pcmanfm
    , ((modMask .|. shiftMask,   xK_f     ), spawn "exe=`kitty --class=vifm vifm`")

    -- launch dmenu
    , ((modMask,               xK_d     ), spawn "exe=`dmenu_run`")

    -- close focused window
    , ((modMask .|. shiftMask, xK_c     ), kill)

     -- Rotate through the available layout algorithms
    , ((modMask,               xK_space ), sendMessage NextLayout)

    --  Reset the layouts on the current workspace to default
    , ((modMask .|. shiftMask, xK_space ), setLayout $ XMonad.layoutHook conf)

    -- Resize viewed windows to the correct size
    , ((modMask,               xK_r     ), refresh)

    -- Connect to vpn
    , ((modMask,               xK_n     ), spawn "exe=`/home/goreorto/.tw-vpn.sh c`")

    -- Disconnect vpn
    , ((modMask .|. shiftMask, xK_n     ), spawn "exe=`/home/goreorto/.tw-vpn.sh d`")

    -- Screenshot region
    , ((modMask ,              xK_Print ), spawn "exe=`scrot -s            -e 'mv $f ~/Downloads/%Y%m%d_%H%M%S_$wx$h.png'`")
    , ((modMask .|. shiftMask, xK_Print ), spawn "exe=`scrot -s -u -c -d 3 -e 'mv $f ~/Downloads/%Y%m%d_%H%M%S_$wx$h.png'`")

    -- logout
    , ((modMask              , xK_Scroll_Lock  ), spawn "exe=`dm-logout`")

    -- Move focus to the next window
	, ((modMask,               xK_Tab   ), windows W.focusUp)
	, ((modMask .|. shiftMask, xK_Tab   ), windows W.focusDown)

    , ((modMask,               xK_j     ), do
        layout <- getActiveLayoutDescription
        case layout of
            "Minimize Spacing BSP" -> sendMessage $ ExpandTowards D
            _ -> sendMessage MirrorShrink
      )

    , ((modMask,               xK_k     ), do
        layout <- getActiveLayoutDescription
        case layout of
            "Minimize Spacing BSP" -> sendMessage $ ExpandTowards U
            _ -> sendMessage MirrorExpand
      )

    , ((modMask,               xK_h     ), do
        layout <- getActiveLayoutDescription
        case layout of
            "Minimize Spacing BSP" -> sendMessage $ ExpandTowards L
            _ -> sendMessage Shrink
      )

    , ((modMask,               xK_l     ), do
        layout <- getActiveLayoutDescription
        case layout of
            "Minimize Spacing BSP" -> sendMessage $ ExpandTowards R
            _ -> sendMessage Expand
      )

    -- Move focus to the master window
    -- , ((modMask .|. shiftMask, xK_m     ), windows W.focusMaster  )

    -- Swap the focused window and the master window
    , ((modMask,               xK_Return), windows W.swapMaster)

    -- Swap the focused window with the next window
    , ((modMask .|. shiftMask, xK_j     ), windows W.swapUp )

    -- Swap the focused window with the previous window
    , ((modMask .|. shiftMask, xK_k     ), windows W.swapDown )

    -- Push window back into tiling
    , ((modMask,               xK_t     ), withFocused $ windows . W.sink)

    -- Increment the number of windows in the master area
    , ((modMask              , xK_comma ), sendMessage (IncMasterN 1))

    -- Deincrement the number of windows in the master area
    , ((modMask              , xK_period), sendMessage (IncMasterN (-1)))

    -- toggle the status bar gap
    -- TODO, update this binding with avoidStruts , ((modMask              , xK_b     ),

    -- Quit xmonad
    --, ((modMask .|. shiftMask, xK_q     ), io (exitWith ExitSuccess))
    --, ((modMask .|. shiftMask, xK_q     ), spawn "xfce4-session-logout")

    -- Restart xmonad
    , ((modMask,               xK_q     ), restart "xmonad" True)

    -- to hide/unhide the panel
    , ((modMask,               xK_b), sendMessage ToggleStruts)
    , ((modMask,               xK_g), toggleFloat)
    , ((modMask .|. shiftMask, xK_h), withFocused (keysResizeWindow (-10, 0) (0, 0)))
    , ((modMask .|. shiftMask, xK_l), withFocused (keysResizeWindow (10, 0) (0, 0)))
    , ((modMask .|. shiftMask, xK_j), withFocused (keysResizeWindow (0, -10) (0, 0)))
    , ((modMask .|. shiftMask, xK_k), withFocused (keysResizeWindow (0, 10) (0, 0)))

	-- media
    , ((modMask,               xK_Pause), spawn "exe=`rhythmbox-client --play-pause`")
    , ((modMask .|. shiftMask, xK_Pause), spawn "exe=`rhythmbox-client --next`")
	-- clipboard
    , ((modMask .|. shiftMask, xK_v), spawn "exe=`xcmenuctrl`")

	-- minimizing
    , ((modMask,               xK_m     ), withFocused minimizeWindow)
    , ((modMask .|. shiftMask, xK_m     ), withLastMinimized maximizeWindowAndFocus)
    ]
    ++

    --
    -- mod-[1..9], Switch to workspace N
    -- mod-shift-[1..9], Move client to workspace N
    --
    [((m .|. modMask, k), windows $ f i)
        | (i, k) <- zip (XMonad.workspaces conf) [xK_1, xK_2]
        , (f, m) <- [(W.greedyView, 0), (W.shift, shiftMask)]]
    ++

    --
    -- mod-{w,e,r}, Switch to physical/Xinerama screens 1, 2, or 3
    -- mod-shift-{w,e,r}, Move client to screen 1, 2, or 3
    --
    [((m .|. modMask, key), screenWorkspace sc >>= flip whenJust (windows . f))
        | (key, sc) <- zip [xK_e, xK_w] [0..]
        , (f, m) <- [(W.view, 0), (W.shift, shiftMask)]]


------------------------------------------------------------------------
-- Mouse bindings: default actions bound to mouse events
--
myMouseBindings (XConfig {XMonad.modMask = modMask}) = M.fromList $

    -- mod-button1, Set the window to floating mode and move by dragging
    [ ((modMask, button1), (\w -> focus w >> mouseMoveWindow w))

    -- mod-button2, Raise the window to the top of the stack
    , ((modMask, button2), (\w -> focus w >> windows W.swapMaster))

    -- mod-button3, Set the window to floating mode and resize by dragging
    , ((modMask, button3), (\w -> focus w >> mouseResizeWindow w))

    -- you may also bind events to the mouse scroll wheel (button4 and button5)
    ]



------------------------------------------------------------------------
-- Window rules:

-- Execute arbitrary actions and WindowSet manipulations when managing
-- a new window. You can use this to, for example, always float a
-- particular program, or have a client always appear on a particular
-- workspace.
--
-- To find the property name associated with a program, use
-- > xprop | grep WM_CLASS
-- and click on the client you're interested in.
--
-- To match on the WM_NAME, you can use 'title' in the same way that
-- 'className' and 'resource' are used below.
--
myManageHook = composeAll
    [ className =? "MPlayer"          --> doFloat
    , className =? "Gimp"             --> doFloat
    , className =? "Rhythmbox"        --> doCenterFloat
    , className =? "Save Image"       --> doCenterFloat
    , className =? "Zenity"           --> doCenterFloat
    , className =? "trayer"           --> doIgnore
    , resource  =? "desktop_window"   --> doIgnore
    , resource  =? "kdesktop"         --> doIgnore ]

myStartupHook = do
    spawnOnce "trayer --edge bottom --align right --SetDockType true --SetPartialStrut true --expand true --monitor 0 --transparent true --alpha 0 --tint 0x283339  --height 32 &"
    spawnOnce "nitrogen --restore /usr/share/background &"
    spawnOnce "picom --experimental-backends &"
    spawnOnce "nm-applet &"
    spawnOnce "blueman-applet &"
    spawnOnce "volumeicon &"
    setWMName "LG3D"

-- Whether focus follows the mouse pointer.

------------------------------------------------------------------------
-- Status bars and logging

-- Perform an arbitrary action on each internal state change or X event.
-- See the 'DynamicLog' extension for examples.
--
-- To emulate dwm's status bar
--
-- > logHook = dynamicLogDzen
--
-- myLogHook = return ()


-- A structure containing your configuration settings, overriding
-- fields in the default config. Any you don't override, will
-- use the defaults defined in xmonad/XMonad/Config.hs
--
-- No need to modify this.
--

spacingAmount = 13

myLayouts = tiled ||| full ||| threecol ||| threecolmid ||| bsp
  where
    tiled       = minimize $ smartSpacing spacingAmount $ smartBorders $ ResizableTall nmaster delta ratio []
    full        = smartSpacing spacingAmount $ smartBorders $ Full
    threecol    = minimize $ smartSpacing spacingAmount $ smartBorders $ ThreeCol nmaster delta ratio
    threecolmid = minimize $ smartSpacing spacingAmount $ smartBorders $ ThreeColMid nmaster delta ratio
    bsp         = minimize $ smartSpacing spacingAmount $ smartBorders $ emptyBSP
    -- grid        = smartSpacing spacingAmount $ smartBorders $ Grid
    nmaster = 1     -- Number of windows in master pane
    ratio   = 1/2   -- Proportion of screen occupied by master pane
    delta   = 3/100 -- Percent of screen to increment by when resizing

main = do
     spawnPipe "xcmenu"
     xmproc <- spawnPipe "xmobar ~/.xmonad/xmobarrc"
     xmonad $ gnomeConfig
        { logHook = dynamicLogWithPP $ xmobarPP
                        { ppOutput = hPutStrLn xmproc
                        , ppTitle = xmobarColor "green" "" . shorten 50
                        }
        , startupHook        = myStartupHook
        , terminal           = "kitty"
        , focusFollowsMouse  = True
        , borderWidth        = 5
        , modMask            = mod4Mask
        , workspaces         = ["web", "dev", "float"]
        , normalBorderColor  = "#181818"
        , focusedBorderColor = "#cc7700"
        , keys               = myKeys
        , mouseBindings      = myMouseBindings
        --, layoutHook         = avoidStruts . smartBorders . onWorkspace "float" simplestFloat $ minimize (tile ||| ThreeCol 1 (3/100) (1/2) ||| ThreeColMid 1 (3/100) (1/2) ||| Full)
        , layoutHook         = avoidStruts $ myLayouts
        , manageHook         = myManageHook <+> manageHook defaultConfig
        }

